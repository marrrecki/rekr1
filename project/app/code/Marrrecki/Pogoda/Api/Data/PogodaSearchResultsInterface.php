<?php


namespace Marrrecki\Pogoda\Api\Data;

interface PogodaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Pogoda list.
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface[]
     */
    public function getItems();

    /**
     * Set temperature list.
     * @param \Marrrecki\Pogoda\Api\Data\PogodaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
