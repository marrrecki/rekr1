<?php


namespace Marrrecki\Pogoda\Api\Data;

interface PogodaInterface
{

    const POGODA_ID = 'pogoda_id';
    const CREATED_AT = 'created_at';
    const TEMPERATURE = 'temperature';

    /**
     * Get pogoda_id
     * @return string|null
     */
    public function getPogodaId();

    /**
     * Set pogoda_id
     * @param string $pogodaId
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setPogodaId($pogodaId);

    /**
     * Get temperature
     * @return string|null
     */
    public function getTemperature();

    /**
     * Set temperature
     * @param string $temperature
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setTemperature($temperature);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setCreatedAt($createdAt);
}
