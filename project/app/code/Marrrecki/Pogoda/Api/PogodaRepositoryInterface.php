<?php


namespace Marrrecki\Pogoda\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PogodaRepositoryInterface
{

    /**
     * Save Pogoda
     * @param \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
    );

    /**
     * Retrieve Pogoda
     * @param string $pogodaId
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($pogodaId);

    /**
     * Retrieve Pogoda matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Marrrecki\Pogoda\Api\Data\PogodaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Pogoda
     * @param \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
    );

    /**
     * Delete Pogoda by ID
     * @param string $pogodaId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pogodaId);
}
