<?php


namespace Marrrecki\Pogoda\Block\Pogoda;

use Magento\Framework\View\Element\Template;

class Pogoda extends Template
{
    const UNIT = '%s&deg;C';

    protected $pogodaModel;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Marrrecki\Pogoda\Model\Pogoda $pogodaModel,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->pogodaModel = $pogodaModel;
    }

    /**
     * @return string
     */
    public function getCurrentTemperature()
    {
        $data = $this->pogodaModel
            ->getCollection()
            ->setOrder('pogoda_id', 'DESC')
            ->setPageSize(1)
            ->getData();    
        return sprintf(self::UNIT, $data[0]['temperature']);
    }
}
