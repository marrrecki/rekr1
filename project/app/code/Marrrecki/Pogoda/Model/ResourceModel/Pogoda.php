<?php


namespace Marrrecki\Pogoda\Model\ResourceModel;

class Pogoda extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('marrrecki_pogoda_pogoda', 'pogoda_id');
    }
}
