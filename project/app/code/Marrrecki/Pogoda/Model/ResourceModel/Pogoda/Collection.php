<?php


namespace Marrrecki\Pogoda\Model\ResourceModel\Pogoda;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Marrrecki\Pogoda\Model\Pogoda::class,
            \Marrrecki\Pogoda\Model\ResourceModel\Pogoda::class
        );
    }
}
