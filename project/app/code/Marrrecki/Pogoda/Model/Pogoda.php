<?php


namespace Marrrecki\Pogoda\Model;

use Marrrecki\Pogoda\Api\Data\PogodaInterface;

class Pogoda extends \Magento\Framework\Model\AbstractModel implements PogodaInterface
{

    protected $_eventPrefix = 'marrrecki_pogoda_pogoda';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Marrrecki\Pogoda\Model\ResourceModel\Pogoda::class);
    }

    /**
     * Get pogoda_id
     * @return string
     */
    public function getPogodaId()
    {
        return $this->getData(self::POGODA_ID);
    }

    /**
     * Set pogoda_id
     * @param string $pogodaId
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setPogodaId($pogodaId)
    {
        return $this->setData(self::POGODA_ID, $pogodaId);
    }

    /**
     * Get temperature
     * @return string
     */
    public function getTemperature()
    {
        return $this->getData(self::TEMPERATURE);
    }

    /**
     * Set temperature
     * @param string $temperature
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setTemperature($temperature)
    {
        return $this->setData(self::TEMPERATURE, $temperature);
    }

    /**
     * Get created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Marrrecki\Pogoda\Api\Data\PogodaInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
