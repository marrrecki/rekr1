<?php


namespace Marrrecki\Pogoda\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Marrrecki\Pogoda\Model\ResourceModel\Pogoda\CollectionFactory as PogodaCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\CouldNotSaveException;
use Marrrecki\Pogoda\Model\ResourceModel\Pogoda as ResourcePogoda;
use Marrrecki\Pogoda\Api\PogodaRepositoryInterface;
use Marrrecki\Pogoda\Api\Data\PogodaSearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Marrrecki\Pogoda\Api\Data\PogodaInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class PogodaRepository implements PogodaRepositoryInterface
{

    protected $dataPogodaFactory;

    protected $resource;

    protected $pogodaCollectionFactory;

    private $storeManager;
    protected $dataObjectProcessor;

    protected $pogodaFactory;

    protected $dataObjectHelper;

    protected $searchResultsFactory;


    /**
     * @param ResourcePogoda $resource
     * @param PogodaFactory $pogodaFactory
     * @param PogodaInterfaceFactory $dataPogodaFactory
     * @param PogodaCollectionFactory $pogodaCollectionFactory
     * @param PogodaSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourcePogoda $resource,
        PogodaFactory $pogodaFactory,
        PogodaInterfaceFactory $dataPogodaFactory,
        PogodaCollectionFactory $pogodaCollectionFactory,
        PogodaSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->pogodaFactory = $pogodaFactory;
        $this->pogodaCollectionFactory = $pogodaCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPogodaFactory = $dataPogodaFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
    ) {
        /* if (empty($pogoda->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $pogoda->setStoreId($storeId);
        } */
        try {
            $this->resource->save($pogoda);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the pogoda: %1',
                $exception->getMessage()
            ));
        }
        return $pogoda;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($pogodaId)
    {
        $pogoda = $this->pogodaFactory->create();
        $this->resource->load($pogoda, $pogodaId);
        if (!$pogoda->getId()) {
            throw new NoSuchEntityException(__('Pogoda with id "%1" does not exist.', $pogodaId));
        }
        return $pogoda;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->pogodaCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Marrrecki\Pogoda\Api\Data\PogodaInterface $pogoda
    ) {
        try {
            $this->resource->delete($pogoda);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Pogoda: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($pogodaId)
    {
        return $this->delete($this->getById($pogodaId));
    }
}
