<?php


namespace Marrrecki\Pogoda\Cron;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;
use Marrrecki\Pogoda\Api\PogodaRepositoryInterface;
use Marrrecki\Pogoda\Model\Pogoda as PogodaModel;
use Magento\Framework\App\Cache\TypeListInterface; 

class Pogoda
{

    const API_URL = 'http://api.openweathermap.org/data/2.5/weather?q=Lublin,Poland&units=metric&lang=pl&apiKey=5956aae1505731f5472adccf1ab1cef9';
    const CACHE_TYPES = ['full_page'];

    protected $logger;
    protected $httpClient;
    protected $pogodaRepository;
    protected $pogodaModel;
    protected $cacheTypeList;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \GuzzleHttp\Client $httpClient
     * @param \Marrrecki\Pogoda\Api\PogodaRepositoryInterface $pogodaRepository
     * @param \Marrrecki\Pogoda\Model\Pogoda $pogodaModel
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        LoggerInterface $logger, 
        Client $httpClient, 
        PogodaRepositoryInterface $pogodaRepository, 
        PogodaModel $pogodaModel,
        TypeListInterface $cacheTypeList
    )
    {
        $this->logger = $logger;
        $this->httpClient = $httpClient;
        $this->pogodaRepository = $pogodaRepository;
        $this->pogodaModel = $pogodaModel;
        $this->cacheTypeList = $cacheTypeList; 
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo("Cronjob Pogoda is executed.");

        $weatherData = $this->getWeatherData();
        if(!empty($weatherData)){
            $this->updateWeather($weatherData);
            $this->clearCache();
        }
    }

    /**
     * @param array $data
     * @return void
     */
    protected function updateWeather(array $data)
    {
        $this->pogodaModel->setTemperature((int)$data['main']['temp']);
        $this->pogodaModel->setCreatedAt(date('Y-m-d H:i:s'));
        $this->pogodaRepository->save($this->pogodaModel);
    }

    /**
     * @return array
     */
    protected function getWeatherData():array
    {
        $data = [];
        try {
            $data = json_decode(
                $this->httpClient->request('GET', self::API_URL)->getBody(),
                true
            );  
        } catch(Exception $e){
            $this->logger->addInfo("Cronjob Pogoda failed: ".$e->getMessage());
        }
        return $data;
    }

    /**
     * @return void
     */
    protected function clearCache()
    {
        foreach (self::CACHE_TYPES as $type) { 
            $this->cacheTypeList->cleanType($type); 
        } 
    }
}
