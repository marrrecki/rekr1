<?php


namespace Marrrecki\Pogoda\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_marrrecki_pogoda_pogoda = $setup->getConnection()->newTable($setup->getTable('marrrecki_pogoda_pogoda'));

        $table_marrrecki_pogoda_pogoda->addColumn(
            'pogoda_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_marrrecki_pogoda_pogoda->addColumn(
            'temperature',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'temperature'
        );

        $table_marrrecki_pogoda_pogoda->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => False],
            'created_at'
        );

        //Your install script

        $setup->getConnection()->createTable($table_marrrecki_pogoda_pogoda);
    }
}
