#instalacja
- Sklonuj repo
- wejdź do katalogu docker i odpal komendę ```docker-compose up```
- wejdź na kontener workspace ```docker-compose exec workspace bash```
- wywołaj komendę ```composer install```
- wywołaj komendę
```php bin/magento setup:install --backend-frontname="admin" --key="admin" --session-save="files" --db-host="mysql" --db-name="default" --db-user="default" --db-password="secret" --base-url="http://localhost/" --admin-user="admin" --admin-password="admin123" --admin-email="example@gmail.com" --admin-firstname="admin" --admin-lastname="admin"```
- zmien uprawnienia do zapisu plików/katalogów ```chmod -R 777 var/* generated/*```
- wywołaj komendę ```php bin/magento deploy:mode:set developer```
- wywołaj komendę ```php bin/magento setup:di:compile```
- zainstaluj crony ```php bin/magento cron:install```
- edytuj crontab ```crontab -e```, usuń z niego komendy i wstaw ```* * * * * /usr/bin/php7.1 /var/www/bin/magento cron:run >> /var/www/var/log/magento.cron.log```
- zaloguj się do panel admina, edytuj stronę główną (Content/Pages Home), wyłącz edytor HTML i wstaw kod ```<p>{{block class="Marrrecki\Pogoda\Block\Pogoda\Pogoda" template="Marrrecki_Pogoda::pogoda/pogoda.phtml"}}</p>```